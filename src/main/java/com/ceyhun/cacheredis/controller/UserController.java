package com.ceyhun.cacheredis.controller;

import com.ceyhun.cacheredis.model.User;
import com.ceyhun.cacheredis.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(("/v1/user"))
public class UserController {
    private final UserService userService;

    @PostMapping("/create")
    public void create(User user) {
        userService.create(user);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }
}